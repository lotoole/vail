<?php
include 'top.php';
?>

<section class = "intro">
  <div class = "container">
    <div class = "row">
      <div class = "col-md-12">
        <?php
        $pageId = $PATH_PARTS['filename'] . '.php';

        $introQuery = "SELECT fldTitle, fldText, fldFileName FROM tblGeneralSections ";
        $introQuery .= "JOIN tblPageIds on tblGeneralSections.fnkPageId=tblPageIds.pmkPageId ";
        $introQuery .= "WHERE fldFileName= '";
        $introQuery .= $pageId;
        $introQuery .= "'";

        // SELECT fldTitle, fldText, fldFileName FROM tblGeneralSections
        // JOIN tblPageIds on tblGeneralSections.fnkPageId=tblPageIds.pmkPageId
        // WHERE fldFileName= 'research.php'

        if ($thisDatabaseReader->querySecurityOk($introQuery, 1,0,2)) {
            // $introQuery = $thisDatabaseReader->sanitizeQuery($introQuery);
            $introRecords = $thisDatabaseReader->select($introQuery, '');
        }

        if(is_array($introRecords)) {
          foreach($introRecords as $introRecord) {
            if($introRecord['fldTitle']) {
              print '<h1>' . $introRecord['fldTitle'] . '</h1>';
            }
            if($introRecord['fldText']) {
              print '<p style = "font-size: 15px;">' . $introRecord['fldText'] . '</p>';

            }
          }
        }
        ?>
      </div>
    </div>
  </div>
</section>


<section class="research-records">
  <div class="container">
    <div class="row">
<?php $records = '';

$query = 'SELECT pmkResearchId, tblResearchProjects.fldTitle, fldText, fldDisplay, fldReference, tblResearchMedia.fnkMediaId, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblResearchProjects ';
$query .= 'LEFT JOIN tblReferences ON tblResearchProjects.pmkResearchId=tblReferences.fnkResearchId ';
$query .= 'LEFT JOIN tblResearchMedia ON tblResearchProjects.pmkResearchId=tblResearchMedia.fnkResearchId ';
$query .= 'LEFT JOIN tblMedia ON tblResearchMedia.fnkMediaId=tblMedia.pmkMediaId ';
$query .= 'WHERE fldDisplay=1';

// SELECT pmkResearchId, tblResearchProjects.fldTitle, fldText, fldDisplay, fldReference, tblResearchMedia.fnkMediaId, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblResearchProjects
// LEFT JOIN tblReferences ON tblResearchProjects.pmkResearchId=tblReferences.fnkResearchId
// LEFT JOIN tblResearchMedia ON tblResearchProjects.pmkResearchId=tblResearchMedia.fnkResearchId
// LEFT JOIN tblMedia ON tblResearchMedia.fnkMediaId=tblMedia.pmkMediaId
// WHERE fldDisplay=1

// NOTE: The full method call would be:
//           $thisDatabaseReader->querySecurityOk($query, 0, 0, 0, 0, 0)
if ($thisDatabaseReader->querySecurityOk($query, 1)) {
    $query = $thisDatabaseReader->sanitizeQuery($query);
    $records = $thisDatabaseReader->select($query, '');
}

if (is_array($records)) {
  // var_dump($records);
  //keep track of which research records have been used before
  $usedRecords = array();
  //keep track of used media items
  $usedMedia = array();
  //For displaying numbers of references printed
  $authorCount = 1;
  //get all the pmk's into an arrray
  $mediaCount = 0;
  $pmk = array();
  foreach ($records as $record) {
      $pmk[] = (int)$record['pmkResearchId'];
  }
  $pmkCount = 0;
  $pmkCountSet = false;
  $usedPmkCount = 0;
    foreach ($records as $record) {
      $displayContent = true;

      //count the number of times the current pmk has been used
      //count the number of times the current pmk shows up in the pmk array
      if($pmkCountSet == false) {
        foreach($pmk as $pm) {
          if((int)$record['pmkResearchId'] == $pm) {
            $pmkCount++;
            $pmkCountSet = true;
          }
        }
      }

      foreach($usedRecords as $usedRecord) {
        // only display author information if reference information displayed already
        if((int)$record['pmkResearchId'] == $usedRecord && $usedPmkCount != $pmkCount) {
          print '<div class="col-lg-12 author">';
          print '<p>' . '<span>' . $authorCount . '.</span>' . ' ' . $record['fldReference'] . '</p>';
          print '</div>';

          $usedPmkCount++;
          $authorCount++;
          $displayContent = false;
          $usedMedia = array();
        } elseif($usedPmkCount == $pmkCount) {
          print '<div class="col-lg-12 author final-author">';
          print '<p>' . '<span>' . $authorCount . '.</span>' . ' ' . $record['fldReference'] . '</p>';
          print '</div>';


          $usedPmkCount = 0;
          $pmkCount = 0;
          $pmkCountSet = false;
          $authorCount++;
          $displayContent = false;
          $usedMedia = array();
        }
      }

      // only display new content
      if($displayContent) {
        $authorCount = 1;
        if($record['fldType'] != null && $record['fldType'] != 'pdf') {
            print '<div class="col-lg-4 media">';
        }


          //loop through used media items
          if(!empty($usedMedia)) {
            // print 'media is array';
            // var_dump($usedMedia);
            foreach($usedMedia as $media) {
              //make sure this media item has not been used
              if((int)$record['pmkMediaId'] != $media) {
                //if video, display it like this
                if($record['fldType'] == 'video') {
                  print '<iframe width="325" height="200" src=" ' . $record['fldSource'] . '" frameborder="0"><p>Your browser does not support iframes.</p></iframe>';
                  print '</div>';
                }
                // if image use this format
                elseif($record['fldType'] == 'image') {
                  print '<img src="media/images/' . $record['fldSource'] . '" alt="' . $record['fldMediaTitle'] . '">';
                  print '</div>';
                }
                // if pdf use this format
                elseif($record['fldType'] == 'pdf') {
                  print '<div class="col-lg-12">';
                  print '<a href="media/pdf/' . $record['fldSource'] . '" target="_blank">' . $record['fldMediaTitle'] . '</a>';
                  print '</div>';
                }
              }
            }
          } else {
            //if video
            if($record['fldType'] == 'video') {
              print '<iframe width="325" height="200" src=" ' . $record['fldSource'] . '" frameborder="0"><p>Your browser does not support iframes.</p></iframe>';
              print '</div>';
            }
            // if image use this format
            elseif($record['fldType'] == 'image') {
              print '<img src="media/images/' . $record['fldSource'] . '" alt="' . $record['fldMediaTitle'] . '">';
              print '</div>';
            }
            // if pdf use this format
            elseif($record['fldType'] == 'pdf') {
              print '<div class="col-lg-12 content">';
              print '<h1>' . $record['fldTitle'] . '</h1>';
              print '<p>' . $record['fldText'] . '</p>';
              print '</div>';
              print '<div class="col-lg-12 pdf-media">';
              print '<a href="media/pdf/' . $record['fldSource'] . '" target="_blank">' . $record['fldMediaTitle'] . '</a>';
              print '</div>';
            }
          }

          if($record['fldType'] != 'pdf' && $record['fldSource'] != null) {
            print '<div class="col-lg-8 content">';
            print '<h1>' . $record['fldTitle'] . '</h1>';
            print '<p>' . $record['fldText'] . '</p>';
            print '</div>';
          } elseif ($record['fldSource'] == null) {
            print '<div class="col-lg-12 content">';
            print '<h1>' . $record['fldTitle'] . '</h1>';
            print '<p>' . $record['fldText'] . '</p>';
            print '</div>';
          }

          if($record['fldReference'] != null && $usedPmkCount != $pmkCount) {
            print '<div class="col-lg-12 author">';
            print '<p>' . '<span>' . $authorCount . '. </span>' . ' ' . $record['fldReference'] . '</p>';
            print '</div>';
          } elseif($usedPmkCount == $pmkCount) {
            print '<div class="col-lg-12 author final-author">';
            print '<p>' . '<span>' . $authorCount . '.</span>' . ' ' . $record['fldReference'] . '</p>';
            print '</div>';
            $usedPmkCount = 0;
            $pmkCount = 0;
            $pmkCountSet = false;
          }

          $authorCount++;
          $usedRecords[] = $record['pmkResearchId'];
          $usedMedia[] = $record['pmkMediaId'];
          $usedPmkCount++;
      }
    }
}
?>
    </div>
  </div>
</section>

<?php
include 'footer.php';
?>
