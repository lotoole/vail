<?php

$query = 'SELECT fnkPageId, fldFileName, fldDisplay, fldPageName FROM tblNavigation ';
$query .= 'JOIN tblPageIds ON tblNavigation.fnkPageId=tblPageIds.pmkPageId ';
$query .= 'WHERE fldDisplay = 1 ';
$query .= 'ORDER BY fldOrder';

if ($thisDatabaseReader->querySecurityOk($query, 1, 1)) {
    $query = $thisDatabaseReader->sanitizeQuery($query);
    $records = $thisDatabaseReader->select($query, '');
}

?>
<!-- ######################     Main Navigation   ########################## -->
<nav class ="desktop">
  <div class="container">
    <ol>
      <?php
      if(is_array($records)) {
        foreach($records as $record) {
          $fileName = substr($record['fldFileName'], 0, strpos($record['fldFileName'], "."));
          print '<li ';
          if ($PATH_PARTS['filename'] == $fileName) {
              print ' class="activePage" ';
          }
          print '><a href="' . $record['fldFileName'] . '">' . $record['fldPageName'] .   '</a></li>';
        }
      }
       ?>
    </ol>
    </div>
</nav>
<!-- #################### Ends Main Navigation    ########################## -->

<!-- ######################     Mobile Navigation   ########################## -->
<nav class ="mobile">
  <div class ="ol-wrap">
    <ol>
      <?php
      if(is_array($records)) {
        foreach($records as $record) {
          print '<li ';
          if ($PATH_PARTS['filename'] == $record['fldFileName']) {
              print ' class="activePage" ';
          }
          print '><a href="' . $record['fldFileName'] . '">' . $record['fldPageName'] .   '</a></li>';
        }
      }
       ?>
      </ol>
    </div>
    <a href="#" class ="mobile-nav-toggle"><i class ="fa fa-bars" aria-hidden="true"></i></a>
</nav>
<!-- #################### Ends Mobile Navigation    ########################## -->
