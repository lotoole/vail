<?php
include 'top.php';
?>


<section class = "intro">
  <div class = "container">
    <div class = "row">
      <div class = "col-md-12">
        <?php
        $pageId = $PATH_PARTS['filename'] . '.php';

        $introQuery = "SELECT fldTitle, fldText, fldFileName FROM tblGeneralSections ";
        $introQuery .= "JOIN tblPageIds on tblGeneralSections.fnkPageId=tblPageIds.pmkPageId ";
        $introQuery .= "WHERE fldFileName= '";
        $introQuery .= $pageId;
        $introQuery .= "'";

        // SELECT fldTitle, fldText, fldFileName FROM tblGeneralSections
        // JOIN tblPageIds on tblGeneralSections.fnkPageId=tblPageIds.pmkPageId
        // WHERE fldFileName= 'publications.php'

        if ($thisDatabaseReader->querySecurityOk($introQuery, 1,0,2)) {
            // $introQuery = $thisDatabaseReader->sanitizeQuery($introQuery);
            $introRecords = $thisDatabaseReader->select($introQuery, '');
        }

        if(is_array($introRecords)) {
          foreach($introRecords as $introRecord) {
            if($introRecord['fldTitle']) {
              print '<h1>' . $introRecord['fldTitle'] . '</h1>';
            }
            if($introRecord['fldText']) {
              print '<p>' . $introRecord['fldText'] . '</p>';
            }
          }
        }
        ?>
      </div>
    </div>
  </div>
</section>

<section class="publications-records">
  <div class="container">
    <div class="row">
<?php $records = '';

$query = 'SELECT pmkPublicationId, fldTitle, fldLocation, fldExcerpt, fldDate, fldCategory, fldDisplay, fldAuthors, tblPublicationMedia.fldPrimaryMedia , tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblPublications ';
$query .= 'LEFT JOIN tblFullAuthors ON tblPublications.pmkPublicationId=tblFullAuthors.fnkPublicationId ';
$query .= 'LEFT JOIN tblPublicationMedia ON tblPublications.pmkPublicationId=tblPublicationMedia.fnkPublicationId ';
$query .= 'LEFT JOIN tblMedia ON tblPublicationMedia.fnkMediaId=tblMedia.pmkMediaId ';
$query .= 'WHERE fldDisplay = 1 ORDER BY fldCategory DESC, fldPrimaryMedia DESC';

// SELECT pmkPublicationId, fldTitle, fldLocation, fldExcerpt, fldDate, fldCategory, fldDisplay, fldAuthors, tblPublicationMedia.fldPrimaryMedia, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblPublications
// LEFT JOIN tblFullAuthors ON tblPublications.pmkPublicationId=tblFullAuthors.fnkPublicationId
// LEFT JOIN tblPublicationMedia ON tblPublications.pmkPublicationId=tblPublicationMedia.fnkPublicationId
// LEFT JOIN tblMedia ON tblPublicationMedia.fnkMediaId=tblMedia.pmkMediaId
// WHERE fldDisplay = 1 ORDER BY fldCategory DESC, fldPrimaryMedia DESC



// NOTE: The full method call would be:
//           $thisDatabaseReader->querySecurityOk($query, 0, 0, 0, 0, 0)
if ($thisDatabaseReader->querySecurityOk($query, 1, 1)) {
    $query = $thisDatabaseReader->sanitizeQuery($query);
    $records = $thisDatabaseReader->select($query, '');
}


if (DEBUG) {
    print '<p>Contents of the array<pre>';
    print_r($records);
    print '</pre></p>';
}
//count the pmks so I know how many media items to display
$mediaCount = 0;
$pmk = array();
foreach ($records as $record) {
    $pmk[] = (int)$record['pmkPublicationId'];
}

if (is_array($records)) {
  // booleans checking if title needs to be displayed
  $patentTitle = false;
  $journalsReferencesTitle = false;
  //array to keep track of used records
  $usedRecords = array();
  //used pmk count
  $usedPmkCount = 0;
    foreach ($records as $record) {
      //reset pmkcount each time
      $pmkCount = 0;
      //initially thinking we will display all content
      $displayContent = true;
            //if title for section has not been displayed, display it
            if($record['fldCategory'] == 'Patents' && !$patentTitle) {
              print '<div class="col-lg-12">';
              print '<h2 class="section-title">' . $record['fldCategory'] . '</h2>';
              print '</div>';
              $patentTitle = true;
            } elseif ($record['fldCategory'] == 'Journals and References' && !$journalsReferencesTitle) {
              print '<div class="col-lg-12">';
              print '<h2 class="section-title">' . $record['fldCategory'] . '</h2>';
              print '</div>';
              $journalsReferencesTitle = true;
            }

            //if there are used pubications, see if this one has been used
            if(!empty($usedRecords)) {
              foreach($usedRecords as $usedRecord) {
                if((int)$record['pmkPublicationId'] == $usedRecord) {
                  //if record content already displayed, just display another media item
                  $displayContent = false;
                  break;
                }
              }
            }
            //count the number of times the current pmk shows up in the pmk array
            foreach($pmk as $pm) {
              if((int)$record['pmkPublicationId'] == $pm) {
                $pmkCount++;
              }
            }

            //check for re used record
            if($displayContent) {
              //display primary media item
              if($record['fldType'] == 'video' && $record['fldPrimaryMedia']) {
                print '<div class="col-lg-4 primary-media">';
                print '<iframe width="325px" height="200px" src=" ' . $record['fldSource'] . '" frameborder="0"><p>Your browser does not support iframes.</p></iframe>';
                print '</div>';
                $usedPmkCount++;
              }
              elseif($record['fldType'] == 'image' && $record['fldPrimaryMedia']) {
                print '<div class="col-lg-4 primary-media">';
                print '<img src="media/images/' . $record['fldSource'] . '" alt="' . $record['fldMediaTitle'] . '">';
                print '</div>';
                $usedPmkCount++;
              }
              print '<div class="col-lg-8 publication">';
              print '<h1>' . $record['fldTitle'] . '</h1>';
              print '<p>' . $record['fldExcerpt'] . '</p>';
              print '<p>' . $record['fldLocation'] . ' | ' . $record['fldDate'] . ' | ' . $record['fldCategory'] .  '</p>';
              print '<p class="authors">' . $record['fldAuthors'] . '</p>';

              print '<h6 class="associated-media">Associated Media</h6>';
              if($record['fldType'] == 'video' && $record['fldPrimaryMedia'] != 1) {
                print '<a class="pubMediaLink" target="_blank" href="' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
              }
              elseif($record['fldType'] == 'image' && $record['fldPrimaryMedia'] != 1) {
                print '<a class="pubMediaLink" target="_blank" href="media/images/' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
              }
              elseif($record['fldType'] == 'pdf' && $record['fldPrimaryMedia'] != 1) {
                print '<a class="pubMediaLink" target="_blank" href="media/pdf/' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
              }

            }

            //if there are used pubications, see if this one has been used
            if(!empty($usedRecords)) {
              foreach($usedRecords as $usedRecord) {
                if((int)$record['pmkPublicationId'] == $usedRecord) {
                  //if record content already displayed, just display another media item
                  $displayContent = false;
                  if($record['fldType'] == 'video') {
                    print '<a class="pubMediaLink" target="_blank" href="' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
                  }
                  elseif($record['fldType'] == 'image') {
                    print '<a class="pubMediaLink" target="_blank" href="media/images/' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
                  }
                  elseif($record['fldType'] == 'pdf') {
                    print '<a class="pubMediaLink" target="_blank" href="media/pdf/' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
                  }
                  $usedPmkCount++;

                  if($usedPmkCount == $pmkCount && $record['fldType'] != 1) {
                    print '</div>';
                    $usedPmkCount = 0;
                  }

                  break;
                }
              }
            }

            $usedRecords[] = $record['pmkPublicationId'];
        }
    }
?>
    </div>
  </div>
</section>

<?php
include 'footer.php';
?>
