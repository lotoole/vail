<?php
include 'top.php';
?>

<section class="datasets-records">
  <div class="container">
    <div class="row gutter-60">
<?php $records = '';

// tblDataSets
// tblDataSetsMedia

$query = 'SELECT pmkDataSetId, fldTitle, fldText, fldCodeName, fldDisplay, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId, tblDataSetsMedia.fldPrimaryMedia FROM tblDataSets ';
// $query .= 'LEFT JOIN tblFullAuthors ON tblPublications.pmkDataSetId=tblFullAuthors.fnkPublicationId ';
$query .= 'LEFT JOIN tblDataSetsMedia ON tblDataSets.pmkDataSetId=tblDataSetsMedia.fnkDataSetId ';
$query .= 'LEFT JOIN tblMedia ON tblDataSetsMedia.fnkMediaId=tblMedia.pmkMediaId ';
$query .= 'WHERE fldDisplay = 1';

//SELECT pmkDataSetId, fldTitle, fldText, fldDisplay, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblDataSets LEFT JOIN tblDataSetsMedia ON tblDataSets.pmkDataSetId=tblDataSetsMedia.fnkDataSetId LEFT JOIN tblMedia ON tblDataSetsMedia.fnkMediaId=tblMedia.pmkMediaId WHERE fldDisplay = 1



// NOTE: The full method call would be:
//           $thisDatabaseReader->querySecurityOk($query, 0, 0, 0, 0, 0)
if ($thisDatabaseReader->querySecurityOk($query, 1)) {
    $query = $thisDatabaseReader->sanitizeQuery($query);
    $records = $thisDatabaseReader->select($query, '');
}

//print the buttons
if (is_array($records)) {
  print '<div class="col-md-12 buttons-wrap">';
  print '<h1>Datasets</h1>';
  // print '<h6>Click the class you want to see more information on</h6>';
  print '<div class="buttons">';
  foreach ($records as $record) {
    $strippedName = preg_replace('/\s+/', '', $record['fldCodeName']);
    print '<a style="margin-right: 10px;" class="btn btn-lg" href="#' . $strippedName . '">' . $record['fldCodeName'] . '</a>';
  }
  print '</div>';
  print '</div>';
}
//count the pmks so I know how many media items to display
$mediaCount = 0;
$pmk = array();
foreach ($records as $record) {
    $pmk[] = (int)$record['pmkDataSetId'];
}

if (is_array($records)) {
    print '<div class="col-lg-12 classes">';

    foreach ($records as $record) {
      $strippedName = preg_replace('/\s+/', '', $record['fldCodeName']);
      print '<div class="class" id="' . $strippedName . '">';
        print '<h6 class="flop-title">' . $record['fldCodeName'] . '</h6>';
        print '<div class ="indented-content">';
          print '<h4>' . $record['fldTitle'] . '</h4>';

          print '<p class="full-text">' . $record['fldText'] . '</p>';
        print '</div>';
        print '</div>';
    }
    print '</div>';
}
?>
    </div>
  </div>
</section>

<?php
include 'footer.php';
?>
