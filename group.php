<?php
include 'top.php';
?>

<section class="director">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Director</h1>
      </div>
      <?php
        $records = '';

        $query = 'SELECT pmkDirectorId, fldName, fldPosition, fldYears, fldText, tblDirectorMedia.fnkMediaId, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblDirector ';
        $query .= 'LEFT JOIN tblDirectorMedia ON tblDirector.pmkDirectorId=tblDirectorMedia.fnkDirectorId ';
        $query .= 'LEFT JOIN tblMedia ON tblDirectorMedia.fnkMediaId=tblMedia.pmkMediaId ';

        // SELECT pmkDirectorId, fldName, fldPosition, fldYears, fldText, tblDirectorMedia.fnkMediaId, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblDirector
        // LEFT JOIN tblDirectorMedia ON tblDirector.pmkDirectorId=tblDirectorMedia.fnkDirectorId
        // LEFT JOIN tblMedia ON tblDirectorMedia.fnkMediaId=tblMedia.pmkMediaId

        if ($thisDatabaseReader->querySecurityOk($query, 0)) {
            $query = $thisDatabaseReader->sanitizeQuery($query);
            $records = $thisDatabaseReader->select($query, '');
        }

        if (is_array($records)) {
          foreach ($records as $record) {
            print '<div class="col-md-4">';
              print '<div class="member">';
                  print '<img width="125" height="125" src="media/images/' . $record['fldSource'] . '">';
              print '<h3>' . $record['fldName'] . '</h3>';
              print '<span>' . $record['fldPosition'] . '</span>';
              print '<span>' . $record['fldYears'] . '</span>';
              print '</div>';
            print '</div>';
            print '<div class="col-md-8">';
              print '<p>' . $record['fldText'] . '</p>';
            print '</div>';
          }
        }
      ?>
    </div>
  </div>
</section>

<section class="team">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Team</h1>
      </div>
      <?php
      $records = '';

      $query = 'SELECT pmkMemberId, fldName, fldPosition, fldDates, tblMembersMedia.fnkMediaId, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId, fldLinkImage FROM tblMembers ';
      $query .= 'LEFT JOIN tblMembersMedia ON tblMembers.pmkMemberId=tblMembersMedia.fnkMemberId ';
      $query .= 'LEFT JOIN tblMedia ON tblMembersMedia.fnkMediaId=tblMedia.pmkMediaId ';

      // SELECT pmkMemberId, fldName, fldPosition, fldDates, tblMembersMedia.fnkMediaId, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblMembers
      // LEFT JOIN tblMembersMedia ON tblMembers.pmkMemberId=tblMembersMedia.fnkMemberId
      // LEFT JOIN tblMedia ON tblMembersMedia.fnkMediaId=tblMedia.pmkMediaId

      if ($thisDatabaseReader->querySecurityOk($query, 0)) {
          $query = $thisDatabaseReader->sanitizeQuery($query);
          $records = $thisDatabaseReader->select($query, '');
      }


    if (is_array($records)) {
      foreach ($records as $record) {
        print '<div class="col-md-4 mb-3">';
          print '<div class="member">';
          if($record['fldLinkImage'] != '') {
            print '<img width="125" height="125" src="' . $record['fldLinkImage'] . '">';
          } else {
              print '<img width="125" height="125" src="media/images/' . $record['fldSource'] . '">';
          }
          print '<h3>' . $record['fldName'] . '</h3>';
          print '<span>' . $record['fldPosition'] . '</span>';
          print '<span>' . $record['fldDates'] . '</span>';
          print '</div>';
        print '</div>';
      }
    }



       ?>
    </div>
  </div>
</section>


<?php
include 'footer.php';
?>
