<?php
include 'top.php';
?>

<section class="publications-records">
  <div class="container">
    <div class="row">
<?php $records = '';

$query = 'SELECT pmkPublicationId, fldTitle, fldLocation, fldExcerpt, fldDate, fldCategory, fldDisplay, fldAuthors, tblPublicationMedia.fldPrimaryMedia , tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId, fldCite FROM tblPublications ';
// $query .= 'LEFT JOIN tblFullAuthors ON tblPublications.pmkPublicationId=tblFullAuthors.fnkPublicationId ';
$query .= 'LEFT JOIN tblPublicationMedia ON tblPublications.pmkPublicationId=tblPublicationMedia.fnkPublicationId ';
$query .= 'LEFT JOIN tblMedia ON tblPublicationMedia.fnkMediaId=tblMedia.pmkMediaId ';
$query .= 'WHERE fldDisplay = 1 ORDER BY fldCategory ASC, fldPrimaryMedia DESC';

// SELECT pmkPublicationId, fldTitle, fldLocation, fldExcerpt, fldDate, fldCategory, fldDisplay, fldAuthors, tblPublicationMedia.fldPrimaryMedia, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId, fldCite FROM tblPublications
// LEFT JOIN tblPublicationMedia ON tblPublications.pmkPublicationId=tblPublicationMedia.fnkPublicationId
// LEFT JOIN tblMedia ON tblPublicationMedia.fnkMediaId=tblMedia.pmkMediaId
// WHERE fldDisplay = 1 ORDER BY fldCategory ASC, fldPrimaryMedia DESC



// NOTE: The full method call would be:
//           $thisDatabaseReader->querySecurityOk($query, 0, 0, 0, 0, 0)
if ($thisDatabaseReader->querySecurityOk($query, 1, 1)) {
    $query = $thisDatabaseReader->sanitizeQuery($query);
    $records = $thisDatabaseReader->select($query, '');
}


if (DEBUG) {
    print '<p>Contents of the array<pre>';
    print_r($records);
    print '</pre></p>';
}
//count the pmks so I know how many media items to display
$mediaCount = 0;
$pmk = array();
foreach ($records as $record) {
    $pmk[] = (int)$record['pmkPublicationId'];
}

//print buttons
print '<div class="col-md-12 buttons-wrap">';
  print '<h1>Publications</h1>';
// print '<h6>Click the class you want to see more information on</h6>';
  print '<div class="buttons">';
    print '<a style="margin-right: 10px;" class="btn btn-lg" href="#journals">Journals</a>';
    print '<a style="margin-right: 10px;" class="btn btn-lg" href="#patents">Patents</a>';
    print '</div>';
print '</div>';

if (is_array($records)) {
  // booleans checking if title needs to be displayed
  $patentTitle = false;
  $journalsReferencesTitle = false;
  //array to keep track of used records
  $usedRecords = array();
  //used pmk count
  $usedPmkCount = 0;
  $pmkCount = 0;
  $pmkCountSet = false;
    foreach ($records as $record) {
      //initially thinking we will display all content
      $displayContent = true;
            //if title for section has not been displayed, display it
            if($record['fldCategory'] == 'Patents' && !$patentTitle) {
              print '<div class="col-lg-12" style="border-top: 1px solid rgba(0, 0, 0, 0.08);">';
              print '<h2 class="section-title" id="patents">' . $record['fldCategory'] . '</h2>';
              print '</div>';
              $patentTitle = true;
            } elseif ($record['fldCategory'] == 'Journals and Conferences' && !$journalsReferencesTitle) {
              print '<div class="col-lg-12">';
              print '<h2 class="section-title" id="journals">' . $record['fldCategory'] . '</h2>';
              print '</div>';
              $journalsReferencesTitle = true;
            }

            //if there are used pubications, see if this one has been used
            if(!empty($usedRecords)) {
              foreach($usedRecords as $usedRecord) {
                if((int)$record['pmkPublicationId'] == $usedRecord) {
                  //if record content already displayed, just display another media item
                  $displayContent = false;
                  break;
                }
              }
            }
            //count the number of times the current pmk shows up in the pmk array
            if($pmkCountSet == false) {
              foreach($pmk as $pm) {
                if((int)$record['pmkPublicationId'] == $pm) {
                  $pmkCount++;
                  $pmkCountSet = true;
                }
              }
            }

            //check for re used record
            if($displayContent) {
              //display primary media item
              if($record['fldType'] == 'video' && $record['fldPrimaryMedia'] == 1) {
                print '<div class="col-lg-2 primary-media">';
                print '<iframe width="325px" height="200px" src=" ' . $record['fldSource'] . '" frameborder="0"><p>Your browser does not support iframes.</p></iframe>';
                print '</div>';
              }
              elseif($record['fldType'] == 'image' && $record['fldPrimaryMedia'] == 1) {
                print '<div class="col-lg-2 primary-media">';
                print '<img src="media/images/' . $record['fldSource'] . '" alt="' . $record['fldMediaTitle'] . '">';
                print '</div>';
              }
              if($record['fldPrimaryMedia'] == 0) {
                print '<div class="col-lg-12 publication">';
              } else {
                print '<div class="col-lg-10 publication">';
              }

              print '<p>' . $record['fldAuthors'] . ', <span style="font-weight: bold;">';
              print   $record['fldTitle'] .  '</span>, ' . $record['fldLocation']  . '</p>';



              if($record['fldType'] == 'video' && $record['fldPrimaryMedia'] != 1) {
                print '<a class="pubMediaLink" target="_blank" href="' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
              }
              elseif($record['fldType'] == 'image' && $record['fldPrimaryMedia'] != 1) {
                print '<a class="pubMediaLink" target="_blank" href="media/images/' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
              }
              elseif($record['fldType'] == 'pdf' && $record['fldPrimaryMedia'] != 1) {
                print '<a class="pubMediaLink" target="_blank" href="media/pdf/' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
              }
              $usedPmkCount++;

            }

            //if there are used pubications, see if this one has been used
            if(!empty($usedRecords)) {
              foreach($usedRecords as $usedRecord) {
                if((int)$record['pmkPublicationId'] == $usedRecord) {
                  //if record content already displayed, just display another media item
                  $displayContent = false;
                  if($record['fldType'] == 'video') {
                    print '<a class="pubMediaLink" target="_blank" href="' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
                  }
                  elseif($record['fldType'] == 'image') {
                    print '<a class="pubMediaLink" target="_blank" href="media/images/' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
                  }
                  elseif($record['fldType'] == 'pdf') {
                    print '<a class="pubMediaLink" target="_blank" href="media/pdf/' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
                  }
                  $usedPmkCount++;
                  break;
                }
              }
            }
            // if this is the final record related to this publication, close it and reset variables
            if($usedPmkCount == $pmkCount) {
              //print the expand button
              // print '<a class="expand-pub">' . '<i class="fa fa-expand" aria-hidden="true"></i>'  . '</a>';
              // print the extra stuff
              // if($record['fldExcerpt']) {
              //   print '<div class="publicationExtra"><p>';
              //   print $record['fldExcerpt'];
              //   print '</p></div>';
              // }
              print '</div>';
              $usedPmkCount = 0;
              $pmkCount = 0;
              $pmkCountSet = false;
            }

            $usedRecords[] = $record['pmkPublicationId'];
        }
    }
?>
    </div>
  </div>
</section>

<?php
include 'footer.php';
?>
