<?php
include 'top.php';

function truncate($string,$length,$append="&hellip;") {
  $string = trim($string);

  if(strlen($string) > $length) {
    $string = wordwrap($string, $length);
    $string = explode("\n", $string, 2);
    $string = $string[0] . $append;
  }

  return $string;
}
?>
<!--########################################## contact section ##########################################-->
<section class="home-slider">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="carousel">
          <?php
            $slides_records_one = '';
            $slides_query = "SELECT pmkResearchId, tblResearchProjects.fldTitle, fldText, fldDisplay, tblResearchMedia.fnkResearchId, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType FROM tblResearchProjects ";
            $slides_query .= "LEFT JOIN tblResearchMedia ON tblResearchProjects.pmkResearchId=tblResearchMedia.fnkResearchId ";
            $slides_query .= "LEFT JOIN tblMedia ON tblResearchMedia.fnkMediaId=tblMedia.pmkMediaId ";
            $slides_query .= "WHERE fldDisplayInSlideShow = 1 ";
            $slides_query .= "ORDER BY fldSlideShowOrder";

            // SELECT pmkResearchId, tblResearchProjects.fldTitle, fldText, fldDisplay, tblResearchMedia.fnkResearchId, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType FROM tblResearchProjects
            // LEFT JOIN tblResearchMedia ON tblResearchProjects.pmkResearchId=tblResearchMedia.fnkResearchId
            // LEFT JOIN tblMedia ON tblResearchMedia.fnkMediaId=tblMedia.pmkMediaId
            // WHERE fldDisplayInSlideShow = 1
            // ORDER BY fldSlideShowOrder

            if ($thisDatabaseReader->querySecurityOk($slides_query, 1, 1)) {
                $slides_query = $thisDatabaseReader->sanitizeQuery($slides_query);
                $slides_records_one = $thisDatabaseReader->select($slides_query, '');
            }

            $slides_query_two = 'SELECT fldTitle, fldText, fldDisplay, tblStudentResearchMedia.fnkMediaId, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblStudentResearchProjects ';
            $slides_query_two .= 'LEFT JOIN tblStudentResearchMedia ON tblStudentResearchProjects.pmkStudentResearchId=tblStudentResearchMedia.fnkStudentResearchId ';
            $slides_query_two .= 'LEFT JOIN tblMedia ON tblStudentResearchMedia.fnkMediaId=tblMedia.pmkMediaId ';
            $slides_query_two .= 'WHERE fldDisplayInSlideShow= 1 ';
            $slides_query_two .= 'ORDER BY fldSlideShowOrder';

            if ($thisDatabaseReader->querySecurityOk($slides_query_two, 1, 1)) {
                $slides_query_two = $thisDatabaseReader->sanitizeQuery($slides_query_two);
                $slides_records_two = $thisDatabaseReader->select($slides_query_two, '');
            }

            $slides_records = array_merge($slides_records_one, $slides_records_two);

            //loop through the contact query
            if(is_array($slides_records)) {
              foreach($slides_records as $record) {
                $media_item;
                $file = $_SERVER['REQUEST_URI'];
                if($record['fldType'] == 'image') {
                  $media_item = $record['fldSource'];
                } else {
                  $media_item = '';
                }
                print '<div class="slide">';
                print '<div class="bg-img" style="background-image: url(' . 'media/images/' . $media_item . ');">';
                print '<a class="slide-link" href="http://swshah.w3.uvm.edu/vail/research.php">';
                  print '<div class="content">';
                  $truncated_title = truncate($record['fldTitle'], 40);
                    print '<h6>' . $truncated_title .  '</h6>';
                    print '</div>';
                  print '</div>';
                  print '</a>';
                print '</div>';
              }
            }
          ?>
        </div>

      </div>
      <div class="col-md-4">
        <div class="contact-info">
          <img src="media/images/safwan-medium.jpg" alt="" width="100%" height="100%">
          <?php
            //contact query
            $contactRecords = '';
            $contactQuery = "SELECT DISTINCT fldTitle, fldOffice, fldOfficePhone, fldEmail, fldLinkedIn, fldGoogleScholar, fldGitLab FROM tblHomePageIntro ";

            if ($thisDatabaseReader->querySecurityOk($contactQuery, 0)) {
                $contactQuery = $thisDatabaseReader->sanitizeQuery($contactQuery);
                $contactRecords = $thisDatabaseReader->select($contactQuery, '');
            }
            //loop through the contact query
            if(is_array($contactRecords)) {
              foreach($contactRecords as $record) {
                //col-md-4 for all its contents
                print '<h6 style="margin: 15px 0 8px 0;">' . $record['fldTitle'] .  '</h6>';
                print '<p>' . 'Dr. Safwan Wshah' . '</p>';
                print '<p><span>Office: </span>' . $record['fldOffice'] . '</p>';
                print '<p><a href="mailto:' . $record['fldEmail'] . '">' . $record['fldEmail'] . '</a></p>';
                print '<p><a href="' . $record['fldGitLab'] . '" target="_blank"><span><i class="fa fa-gitlab" style="margin-right: 8px;"></i>Git Lab</span></a>' . '</p>';
                print '<p><a href="' . $record['fldLinkedIn'] . '" target="_blank"><span><i class="fa fa-linkedin-square" aria-hidden="true" style="margin-right: 8px;"></i>LinkedIn</span></a>' . '</p>';
                print '<p><a href="' . $record['fldGoogleScholar'] . '" target="_blank"><span><i class="fa fa-google-plus" aria-hidden="true" style="margin-right: 8px;"></i>GoogleScholar</span></a>' . '</p>';
              }
            }
          ?>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="join-news">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h3 style="margin-bottom: 0.8em;">About Us</h3>
        <p>We are looking for bright hardworking students at both undergraduate and graduate levels interested in research and scholarship opportunities. If you are interested in working with us, please email us a copy of your CV/Resume @ safwan.wshah@uvm.edu.</p>
      </div>
      <div class="col-md-4">
        <h3 style="margin-bottom: 0.8em;">News</h3>
        <?php $news_records = '';

        $news_query = 'SELECT fldLink, fldDate, fldText FROM tblNews ORDER BY fldOrder';

        // NOTE: The full method call would be:
        //           $thisDatabaseReader->querySecurityOk($query, 0, 0, 0, 0, 0)
        if ($thisDatabaseReader->querySecurityOk($news_query, 0, 1)) {
            $news_query = $thisDatabaseReader->sanitizeQuery($news_query);
            $news_records = $thisDatabaseReader->select($news_query, '');

        }

        if (is_array($news_records)) {
            foreach ($news_records as $record) {
                print '<div class="newsItem">';
                print '<i class="fa fa-envelope-open-o" aria-hidden="true"></i>';
                print '<span class="date"> ' . $record['fldDate'] . ' - ' . '</span>';
                print '<span>' . $record['fldText'] . ' ' . '</span>';
                if($record['fldLink'] != null) {
                    print '<a class="link" href="' . $record['fldLink'] . '">Link To Article</a>';
                }
                print '</div>';
            }
        } ?>
      </div>
    </div>
  </div>
</section>



<!-- <section class="home-intro">
  <div class="container">
    <div class="row">
      <?php
      // $introRecords = '';
      // $pageId = $PATH_PARTS['filename'] . '.php';
      //
      // $introQuery = "SELECT tblGeneralSections.fldTitle, tblGeneralSections.fldText, fldFileName, tblGeneralSections.fldOrder, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.fldSource  FROM tblGeneralSections ";
      // $introQuery .= "JOIN tblPageIds on tblGeneralSections.fnkPageId=tblPageIds.pmkPageId ";
      // $introQuery .= "LEFT JOIN tblGeneralMedia on tblGeneralSections.pmkSectionId=tblGeneralMedia.fnkSectionId ";
      // $introQuery .= "LEFT JOIN tblMedia on tblGeneralMedia.fnkMediaId=tblMedia.pmkMediaId ";
      // $introQuery .= "WHERE fldFileName= '";
      // $introQuery .= $pageId;
      // $introQuery .= "' ";
      // $introQuery .= "AND fldOrder = 1 ";
      //
      // if ($thisDatabaseReader->querySecurityOk($introQuery, 1,1,2)) {
      //     $introRecords = $thisDatabaseReader->select($introQuery, '');
      // }
      // if(is_array($introRecords)) {
      //   foreach($introRecords as $record) {
      //     //col-md-4 for the image
      //     print '<div class="col-md-4">';
      //     print '<img src="media/images/' . $record['fldSource'] . '" alt="' . $record['fldMediaTitle'] . '">';
      //     print '</div>';
      //     //col-md-8 for the title and text
      //     print '<div class="col-md-8">';
      //     print '<h6>' . $record['fldTitle'] . '</h6>';
      //     print '<p>' . $record['fldText'] . '</p>';
      //     print '</div>';
      //   }
      // }


      ?>
      <?php
      //contact query
      // $contactRecords = '';
      // $contactQuery = "SELECT DISTINCT fldTitle, fldOffice, fldOfficePhone, fldEmail, fldLinkedIn, fldGoogleScholar FROM tblHomePageIntro ";
      //
      // if ($thisDatabaseReader->querySecurityOk($contactQuery, 0)) {
      //     $contactQuery = $thisDatabaseReader->sanitizeQuery($contactQuery);
      //     $contactRecords = $thisDatabaseReader->select($contactQuery, '');
      // }
      // //loop through the contact query
      // if(is_array($contactRecords)) {
      //   foreach($contactRecords as $record) {
      //     //col-md-4 for all its contents
      //     print '<div class="col-md-4">';
      //     print '<h6 style="margin-top: 20px;">' . $record['fldTitle'] .  '</h6>';
      //     print '<p><span>Office: </span>' . $record['fldOffice'] . '</p>';
      //     print '<p><span>Email: </span><a href="mailto:' . $record['fldEmail'] . '">' . $record['fldEmail'] . '</a></p>';
      //     print '<p><span><i class="fa fa-linkedin-square" aria-hidden="true" style="margin-right: 8px;"></i>LinkedIn: </span><a href="' . $record['fldLinkedIn'] . '" target="_blank">Click Here</a>' . '</p>';
      //     print '<p><span><i class="fa fa-google-plus" aria-hidden="true" style="margin-right: 8px;"></i>GoogleScholar: </span><a href="' . $record['fldGoogleScholar'] . '" target="_blank">Click Here</a>' . '</p>';
      //     print '</div>';
      //   }
      // }
      //
      // $prospectiveStudentRecords = '';
      // //prospective students query
      // $introQuery = "SELECT tblGeneralSections.fldTitle, tblGeneralSections.fldText, fldFileName, tblGeneralSections.fldOrder, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.fldSource  FROM tblGeneralSections ";
      // $introQuery .= "JOIN tblPageIds on tblGeneralSections.fnkPageId=tblPageIds.pmkPageId ";
      // $introQuery .= "LEFT JOIN tblGeneralMedia on tblGeneralSections.pmkSectionId=tblGeneralMedia.fnkSectionId ";
      // $introQuery .= "LEFT JOIN tblMedia on tblGeneralMedia.fnkMediaId=tblMedia.pmkMediaId ";
      // $introQuery .= "WHERE fldFileName= '";
      // $introQuery .= $pageId;
      // $introQuery .= "' ";
      // $introQuery .= "AND fldTitle = 'Prospective Students'";
      //
      // if ($thisDatabaseReader->querySecurityOk($introQuery, 1,1,4)) {
      //     $prospectiveStudentRecords = $thisDatabaseReader->select($introQuery, '');
      // }
      //
      // if(is_array($prospectiveStudentRecords)) {
      //   foreach($prospectiveStudentRecords as $record) {
      //     print '<div class="col-md-8">';
      //     print '<h6 style="margin-top: 20px;">' . $record['fldTitle'] . '</h6>';
      //     print '<p>' . $record['fldText'] . '</p>';
      //     print '<a class="btn btn-lg" href="mailto:safwan.wshah@uvm.edu">Get In Touch</a>';
      //     print '</div>';
      //   }
      // }


      ?>

    </div>
  </div>
</section> -->
<!--########################################## news section ##########################################-->
<!-- <section class="news">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3>News</h3>
        <?php $news_records = '';

        $news_query = 'SELECT fldLink, fldDate, fldText FROM tblNews ORDER BY fldOrder';

        // NOTE: The full method call would be:
        //           $thisDatabaseReader->querySecurityOk($query, 0, 0, 0, 0, 0)
        if ($thisDatabaseReader->querySecurityOk($news_query, 0, 1)) {
            $news_query = $thisDatabaseReader->sanitizeQuery($news_query);
            $news_records = $thisDatabaseReader->select($news_query, '');

        }

        if (is_array($news_records)) {
            foreach ($news_records as $record) {
                print '<div class="newsItem">';
                print '<i class="fa fa-envelope-open-o" aria-hidden="true"></i>';
                print '<span class="date"> ' . $record['fldDate'] . ' - ' . '</span>';
                print '<span>' . $record['fldText'] . ' ' . '</span>';
                if($record['fldLink'] != null) {
                    print '<a class="link" href="' . $record['fldLink'] . '">Link To Article</a>';
                }
                print '</div>';
            }
        } ?>
      </div>
    </div>
  </div>
</section> -->
<?php
include 'footer.php';
?>
