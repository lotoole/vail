<?php
include 'top.php';

function truncate($string,$length=15,$append="&hellip;") {
  $string = trim($string);

  if(strlen($string) > $length) {
    $string = wordwrap($string, $length);
    $string = explode("\n", $string, 2);
    $string = $string[0] . $append;
  }

  return $string;
}
?>

<section class="classes-records">
  <div class="container">
    <div class="row gutter-60">
<?php $records = '';

$query = 'SELECT fldName, fldDescrpition, fldGoals, fldFullName, fldOrder, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId  FROM tblClasses ';
$query .= 'LEFT JOIN tblClassMedia ON tblClasses.pmkClassId=tblClassMedia.fnkClassId ';
$query .= 'LEFT JOIN tblMedia ON tblClassMedia.fnkMediaId=tblMedia.pmkMediaId ';
$query .= 'ORDER BY fldOrder';

// SELECT fldName, fldDescrpition, fldGoals, fldOrder, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId  FROM tblClasses
// LEFT JOIN tblClassMedia ON tblClasses.pmkClassId=tblClassMedia.fnkClassId
// LEFT JOIN tblMedia ON tblClassMedia.fnkMediaId=tblMedia.pmkMediaId
// ORDER BY fldOrder

// NOTE: The full method call would be:
//           $thisDatabaseReader->querySecurityOk($query, 0, 0, 0, 0, 0)
if ($thisDatabaseReader->querySecurityOk($query, 0, 1)) {
    $query = $thisDatabaseReader->sanitizeQuery($query);
    $records = $thisDatabaseReader->select($query, '');
}
//print the buttons
if (is_array($records)) {
  print '<div class="col-md-12 buttons-wrap">';
  print '<h1>Courses</h1>';
  // print '<h6>Click the class you want to see more information on</h6>';
  print '<div class="buttons">';
  foreach ($records as $record) {
    $strippedName = preg_replace('/\s+/', '', $record['fldName']);
    print '<a style="margin-right: 10px;" class="btn btn-lg" href="#' . $strippedName . '">' . $record['fldName'] . '</a>';
  }
  print '</div>';
  print '</div>';
}

// print the classes
if (is_array($records)) {
    print '<div class="col-lg-8 classes">';

    foreach ($records as $record) {
      $strippedName = preg_replace('/\s+/', '', $record['fldName']);
      print '<div class="class" id="' . $strippedName . '">';
        print '<h6 class="flop-title">' . $record['fldName'] . '</h6>';
        print '<div class ="indented-content">';
          print '<h4>' . $record['fldFullName'] . '</h4>';

          print '<h6>' . 'Description' . '</h6>';
          print '<p class="full-text">' . $record['fldDescrpition'] . '</p>';
          print '<h6>' . 'Goals' . '</h6>';
          print '<p class="full-text">' . $record['fldGoals'] . '</p>';
        print '</div>';
        print '</div>';
    }
    print '</div>';
}
?>
    <!-- query for the sidebar content -->
    <div class="col-lg-4">
      <section class="sidebar">
        <h1>Recent Research Projects</h1>
        <?php $sidebarRecords = '';

        $sidebarQuery = 'SELECT fldTitle, fldText, fldDisplay, tblStudentResearchMedia.fnkMediaId, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblStudentResearchProjects ';
        $sidebarQuery .= 'LEFT JOIN tblStudentResearchMedia ON tblStudentResearchProjects.pmkStudentResearchId=tblStudentResearchMedia.fnkStudentResearchId ';
        $sidebarQuery .= 'LEFT JOIN tblMedia ON tblStudentResearchMedia.fnkMediaId=tblMedia.pmkMediaId ';
        $sidebarQuery .= 'WHERE fldDisplaySidebar= 1 ';
        $sidebarQuery .= 'LIMIT 3';



        // NOTE: The full method call would be:
        //           $thisDatabaseReader->querySecurityOk($sidebarQuery, 0, 0, 0, 0, 0)
        if ($thisDatabaseReader->querySecurityOk($sidebarQuery, 1)) {
            $sidebarQuery = $thisDatabaseReader->sanitizeQuery($sidebarQuery);
            $sidebarRecords = $thisDatabaseReader->select($sidebarQuery, '');
        }
        ?>

        <?php if(is_array($sidebarRecords)) {
          foreach($sidebarRecords as $record) {
            // if there is media, display it
            if($record['fldSource'] != null && $record['fldType'] != 'pdf') {
              print '<div class="sidebar-image">';
              //if video
              if($record['fldType'] == 'video') {
                print '<iframe width="325" height="200" src=" ' . $record['fldSource'] . '" frameborder="0"><p>Your browser does not support iframes.</p></iframe>';
                print '</div>';
              }
              // if image use this format
              elseif($record['fldType'] == 'image') {
                print '<img src="media/images/' . $record['fldSource'] . '" alt="' . $record['fldMediaTitle'] . '">';
                print '</div>';
              }
            }
            //display record content
            if($record['fldType'] == 'pdf') {
              print '<div class="sidebar-content">';
              print '<p>' . $record['fldTitle'] . '</p>';
              // $string = (strlen($record['fldText']) > 13) ? substr($record['fldText'],0,10).'...' : $record['fldText'];
              $string = truncate($record['fldText']);
              print $string;
              // print '<p>' . $record['fldText'] . '</p>';
              print '<a class="pdf" href="media/pdf/' . $record['fldSource'] . '" target="_blank">' . $record['fldMediaTitle'] . '</a>';
              print '</div>';
            } else {
              print '<div class="sidebar-content">';
              print '<h6>' . $record['fldTitle'] . '</h6>';
              print '<p>' . $record['fldText'] . '</p>';
              print '<a href="research.php">More</a>';
              print '</div>';
            }
          }
        }
        print '</div>';
        ?>
      </section>
    </div>
    </div>
  </div>
</section>
<?php
include 'footer.php';
?>
