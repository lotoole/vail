<?php
include 'top.php';
?>

<section class="publications-records">
  <div class="container">
    <div class="row">
<?php $records = '';

// tblDataSets
// tblDataSetsMedia


$query = 'SELECT pmkDataSetId, fldTitle, fldText, fldCodeName, fldDisplay, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId, tblDataSetsMedia.fldPrimaryMedia FROM tblDataSets ';
// $query .= 'LEFT JOIN tblFullAuthors ON tblPublications.pmkDataSetId=tblFullAuthors.fnkPublicationId ';
$query .= 'LEFT JOIN tblDataSetsMedia ON tblDataSets.pmkDataSetId=tblDataSetsMedia.fnkDataSetId ';
$query .= 'LEFT JOIN tblMedia ON tblDataSetsMedia.fnkMediaId=tblMedia.pmkMediaId ';
$query .= 'WHERE fldDisplay = 1';

//SELECT pmkDataSetId, fldTitle, fldText, fldDisplay, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblDataSets LEFT JOIN tblDataSetsMedia ON tblDataSets.pmkDataSetId=tblDataSetsMedia.fnkDataSetId LEFT JOIN tblMedia ON tblDataSetsMedia.fnkMediaId=tblMedia.pmkMediaId WHERE fldDisplay = 1



// NOTE: The full method call would be:
//           $thisDatabaseReader->querySecurityOk($query, 0, 0, 0, 0, 0)
if ($thisDatabaseReader->querySecurityOk($query, 1)) {
    $query = $thisDatabaseReader->sanitizeQuery($query);
    $records = $thisDatabaseReader->select($query, '');
}


if (DEBUG) {
    print '<p>Contents of the array<pre>';
    print_r($records);
    print '</pre></p>';
}
//count the pmks so I know how many media items to display
$mediaCount = 0;
$pmk = array();
foreach ($records as $record) {
    $pmk[] = (int)$record['pmkDataSetId'];
}

if (is_array($records)) {
  //array to keep track of used records
  $usedRecords = array();
  //used pmk count
  $usedPmkCount = 0;
  $pmkCount = 0;
  $pmkCountSet = false;
    foreach ($records as $record) {
      //initially thinking we will display all content
      $displayContent = true;
            //if there are used pubications, see if this one has been used
            if(!empty($usedRecords)) {
              foreach($usedRecords as $usedRecord) {
                if((int)$record['pmkDataSetId'] == $usedRecord) {
                  //if record content already displayed, just display another media item
                  $displayContent = false;
                  break;
                }
              }
            }
            //count the number of times the current pmk shows up in the pmk array
            if($pmkCountSet == false) {
              foreach($pmk as $pm) {
                if((int)$record['pmkDataSetId'] == $pm) {
                  $pmkCount++;
                  $pmkCountSet = true;
                }
              }
            }

            //check for re used record
            if($displayContent) {
              //display primary media item
              if($record['fldType'] == 'video' && $record['fldPrimaryMedia'] == 1) {
                print '<div class="col-lg-2 primary-media">';
                print '<iframe width="325px" height="200px" src=" ' . $record['fldSource'] . '" frameborder="0"><p>Your browser does not support iframes.</p></iframe>';
                print '</div>';
              }
              elseif($record['fldType'] == 'image' && $record['fldPrimaryMedia'] == 1) {
                print '<div class="col-lg-2 primary-media">';
                print '<img src="media/images/' . $record['fldSource'] . '" alt="' . $record['fldMediaTitle'] . '">';
                print '</div>';
              }
              if($record['fldPrimaryMedia'] == 0) {
                print '<div class="col-lg-12 publication">';
              } else {
                print '<div class="col-lg-10 publication">';
              }
              print '<h1>' . $record['fldTitle'] . '</h1>';
              print '<p>' . $record['fldText'] . '</p>';


              if($record['fldType'] == 'video' && $record['fldPrimaryMedia'] != 1) {
                print '<a class="pubMediaLink" target="_blank" href="' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
              }
              elseif($record['fldType'] == 'image' && $record['fldPrimaryMedia'] != 1) {
                print '<a class="pubMediaLink" target="_blank" href="media/images/' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
              }
              elseif($record['fldType'] == 'pdf' && $record['fldPrimaryMedia'] != 1) {
                print '<a class="pubMediaLink" target="_blank" href="media/pdf/' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
              }
              $usedPmkCount++;

            }

            //if there are used pubications, see if this one has been used
            if(!empty($usedRecords)) {
              foreach($usedRecords as $usedRecord) {
                if((int)$record['pmkDataSetId'] == $usedRecord) {
                  //if record content already displayed, just display another media item
                  $displayContent = false;
                  if($record['fldType'] == 'video') {
                    print '<a class="pubMediaLink" target="_blank" href="' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
                  }
                  elseif($record['fldType'] == 'image') {
                    print '<a class="pubMediaLink" target="_blank" href="media/images/' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
                  }
                  elseif($record['fldType'] == 'pdf') {
                    print '<a class="pubMediaLink" target="_blank" href="media/pdf/' . $record['fldSource'] . '">[' . $record['fldMediaTitle'] . ']</a>';
                  }
                  $usedPmkCount++;
                  break;
                }
              }
            }
            // if this is the final record related to this publication, close it and reset variables
            if($usedPmkCount == $pmkCount) {
              //print the expand button
              // print '<a class="expand-pub">' . '<i class="fa fa-expand" aria-hidden="true"></i>'  . '</a>';
              // print the extra stuff
              // if($record['fldExcerpt']) {
              //   print '<div class="publicationExtra"><p>';
              //   print $record['fldExcerpt'];
              //   print '</p></div>';
              // }
              print '</div>';
              $usedPmkCount = 0;
              $pmkCount = 0;
              $pmkCountSet = false;
            }

            $usedRecords[] = $record['pmkDataSetId'];
        }
    }
?>
    </div>
  </div>
</section>

<?php
include 'footer.php';
?>
