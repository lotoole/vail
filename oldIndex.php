<?php
include 'top.php';
?>
<!--########################################## contact section ##########################################-->
<section class="home-intro">
  <div class="container">
    <div class="row">
      <?php

      $contactQuery = "SELECT DISTINCT fldAbout, fldTitle, fldOffice, fldOfficePhone, fldEmail, fldLinkedIn, fldGoogleScholar, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblHomePageIntro ";
      $contactQuery .= "LEFT JOIN tblMedia ON tblHomePageIntro.fnkMediaId=tblMedia.pmkMediaId";

      // SELECT DISTINCT fldTitle, fldOffice, fldOfficePhone, fldEmail, fldLinkedIn, fldGoogleScholar, tblMedia.fldSource, tblMedia.fldMediaTitle, tblMedia.fldType, tblMedia.pmkMediaId FROM tblContactInformation
      // LEFT JOIN tblMedia ON tblContactInformation.fnkMediaId=tblMedia.pmkMediaId

      if ($thisDatabaseReader->querySecurityOk($contactQuery, 0)) {
          $contactQuery = $thisDatabaseReader->sanitizeQuery($contactQuery);
          $contactRecords = $thisDatabaseReader->select($contactQuery, '');
      }

      if(is_array($contactRecords)) {
        foreach($contactRecords as $record) {
          //if image comes through, display it
          print '<div class="col-md-4">';
          if($record['fldSource'] != null && $record['fldType'] == 'image') {
            print '<img src="media/images/' . $record['fldSource'] . '" alt="' . $record['fldMediaTitle'] . '">';
            //display contact information
            print '<h6 style="margin-top: 20px;">' . $record['fldTitle'] .  '</h6>';

            print '<p><span>Office: </span>' . $record['fldOffice'] . '</p>';
            print '<p><span>Email: </span><a href="mailto:' . $record['fldEmail'] . '">' . $record['fldEmail'] . '</a></p>';
            print '<p><span><i class="fa fa-linkedin-square" aria-hidden="true" style="margin-right: 8px;"></i>LinkedIn: </span><a href="' . $record['fldLinkedIn'] . '" target="_blank">Click Here</a>' . '</p>';
            print '<p><span><i class="fa fa-google-plus" aria-hidden="true" style="margin-right: 8px;"></i>GoogleScholar: </span><a href="' . $record['fldGoogleScholar'] . '" target="_blank">Click Here</a>' . '</p>';
          }
          print '</div>';
          //display contact information
          print '<div class="col-md-8">';
          //display about section
          print '<h6>About</h6>';
          print '<p>' . $record['fldAbout'] . '</p>';

          print '</div>';
        }
      }
      ?>
    </div>
  </div>
</section>
<!--########################################## home-general ##########################################-->
<section class ="home-general">
  <div class ="container">
    <div class ="row">
      <?php
      $pageId = $PATH_PARTS['filename'] . '.php';

      $introQuery = "SELECT fldTitle, fldText, fldFileName, fldOrder FROM tblGeneralSections ";
      $introQuery .= "JOIN tblPageIds on tblGeneralSections.fnkPageId=tblPageIds.pmkPageId ";
      $introQuery .= "WHERE fldFileName= '";
      $introQuery .= $pageId;
      $introQuery .= "' ";
      $introQuery .= "AND fldOrder = 1 ";
      $introQuery .= "ORDER BY fldOrder";

      if ($thisDatabaseReader->querySecurityOk($introQuery, 1,1,2)) {
          $introRecords = $thisDatabaseReader->select($introQuery, '');
      }

      if(is_array($introRecords)) {
        foreach($introRecords as $introRecord) {
          if($introRecord['fldTitle']) {
            print '<div class="col-md-4">';
            print '<h3>' . $introRecord['fldTitle'] . '</h3>';
            print '</div>';
          }
          if($introRecord['fldText'] && $introRecord['fldTitle'] != 'Prospective Students') {
            print '<div class="col-md-8">';
            print '<p>' . $introRecord['fldText'] . '</p>';
            print '</div>';
          }
          if($introRecord['fldTitle'] == 'Prospective Students') {
            print '<div class="col-md-8">';
            print '<p>' . $introRecord['fldText'] . '</p>';
            print '<a class="btn btn-lg" href="mailto:safwan.wshah@uvm.edu">Get In Touch</a>';
            print '</div>';
          }
        }
      }
      ?>
    </div>
  </div>
</section>


<!--########################################## news section ##########################################-->
<section class="news">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3>News</h3>
        <?php $news_records = '';

        $news_query = 'SELECT fldLink, fldDate, fldText FROM tblNews ORDER BY fldOrder';

        // NOTE: The full method call would be:
        //           $thisDatabaseReader->querySecurityOk($query, 0, 0, 0, 0, 0)
        if ($thisDatabaseReader->querySecurityOk($news_query, 0, 1)) {
            $news_query = $thisDatabaseReader->sanitizeQuery($news_query);
            $news_records = $thisDatabaseReader->select($news_query, '');

        }

        if (is_array($news_records)) {
            foreach ($news_records as $record) {
                print '<div class="newsItem">';
                print '<i class="fa fa-envelope-open-o" aria-hidden="true"></i>';
                print '<span class="date"> ' . $record['fldDate'] . ' - ' . '</span>';
                print '<span>' . $record['fldText'] . ' ' . '</span>';
                if($record['fldLink'] != null) {
                    print '<a class="link" href="' . $record['fldLink'] . '">Link To Article</a>';
                }
                print '</div>';
            }
        } ?>
      </div>
    </div>
  </div>
</section>
<?php
include 'footer.php';
?>
