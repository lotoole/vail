/* $ */
var WSHAH = WSHAH || {};

WSHAH.init_mobile_nav = function () {
    var toggle = $('.mobile-nav-toggle');

    toggle.click(function (e) {
        e.preventDefault();
        $('html').toggleClass('mobile-nav-active');
    });

};

WSHAH.init_publication_full = function () {
  var toggle = $('.expand-pub');
  var itsParent = $('.expand-pub').parent();

  toggle.click(function (e) {
      e.preventDefault();
      $(this).parent().toggleClass('expand-publication');
  });
}

WSHAH.init_smooth_scroll = function () {
  $(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});
};

WSHAH.init_home_slider = function () {
  var slider = $('.home-slider .carousel');

  slider.slick({
    dots: true,
    prevArrow: '<a class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>',
    nextArrow: '<a class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>',
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToShow: 1,
    swipeToSlide: true,
    centerMode: true,
    centerPadding: 0,
    infinite: true
  });
};


$(document).ready(function () {
  WSHAH.init_mobile_nav();
  WSHAH.init_publication_full();
  WSHAH.init_smooth_scroll();
  WSHAH.init_home_slider();
});
