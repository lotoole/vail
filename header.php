<!-- %%%%%%%%%%%%%%%%%%%%%%     Page header   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->
<header>
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="name">
            <img src="media/images/vail-logo.png" width="250" alt="">
          </div>
        </div>
        <div class="col-md-6">
          <div class="logo">
          <a href="https://www.uvm.edu/"><img src="media/images/uvmlogo2017.svg" width="250" height="250" alt=""></a>
          </div>
        </div>
      </div>
    </div>
</header>
<!-- %%%%%%%%%%%%%%%%%%%%% Ends Page header   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->
